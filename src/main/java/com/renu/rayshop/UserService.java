/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.rayshop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ray
 */
public class UserService {
    private static ArrayList<Product> productList = null;
    static {
        load();
        productList = new ArrayList<>();
        // Mock data
        productList.add(new Product("ID","Name","Brand",22,12));
        productList.add(new Product("ID2","Name2","Brand2",22,12));
        productList.add(new Product("ID2","Name3","Brand3",22,12));
     }
    //create (C)
    public static boolean addProduct(Product product){
        productList.add(product);
        save();
        return true;
    }
    // Delete (D)
    public static boolean delProduct(Product product){
        productList.remove(product);
        save();
        return true;
    }
    public static boolean delProduct(int index){
        productList.remove(index);
       save();
        return true;
    }
    //read (R)
    public static ArrayList<Product> getProduct(){
        return productList;
    }  
    public static Product getProduct(int index){
        return productList.get(index);
    
    }
    public static boolean delAll(){
        save();
        return productList.removeAll(productList);
    }
    // Update (U)
    public static boolean updateProduct(int index, Product product){
        productList.set(index, product);
        save();
        return true;
    }
    
   public static boolean clear(){
        return productList.removeAll(productList);
   
   
   }
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Product.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Product.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
}
}




